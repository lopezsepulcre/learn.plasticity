#' Perform model selection
#'
#' Perform model selection on a specified full model (LM, LMM, GLM, GLMM) by calculating
#' the AICs and producing a table of the best models within a given deltaAIC. It also gives
#' the final/reduced model as the model, within those, that has the most parameters and
#' lowest AIC. Model selection is done sequentially using function MuMIn::dredge.
#'
#' @param model A full model fit with all the terms to consider.
#' @param delta The deltaAIC cutoff for the AIC table. Defaults to 2.
#' @export
#'
model_selection <- function (model, delta = 2){

  # Model selection
  AICtable_full <- MuMIn::dredge(model)

  top_models_index <- which(AICtable_full[,'delta'] <= delta)
  AICtable <- AICtable_full[top_models_index,]

  model_list <-MuMIn::get.models(AICtable, subset = delta <= delta)

  final_model_index <- which.max(unlist(lapply(lapply(model_list, coefficients), length)))

  final_model <- model_list[[final_model_index]]

  return(list(AIC_table = AICtable,
              final_model = final_model))
}


#' Extract effects from model formula for plotting
#'
#' This function extracts, from a given model, the model terms, and separates them into
#' two vectors: (1) with the interaction terms, and (2) with the main effects that do
#' NOT appear in interactions.
#'
#' @param model The model to extract terms from.
#' @return A list of the two vectors mentioned above
#' @return A list of two elements: the AIC table, and the final model.
#' @export
#'
extract_terms <- function(model){
  # Split RHS formula into its components
  terms <- unlist(str_split(as.character(model$terms[[3]][2]), ' +'))
  terms <- terms[which(terms!= '+')]

  # Separate interactions from main effects
  interactions <- terms[grepl(':',terms)]
  mains <- terms[!grepl(':',terms)]

  # Eliminate from the main effects those that are inlcuded in interactions
  int_mains <- sort(unique(unlist(str_split(interactions, ':'))))
  main_only <- mains[which(!mains %in% int_mains)]

  return(list(main = mains, main_only = main_only, int = int_mains))
}

#' Tukey Test of the interacting factors.
#'
#' Collapses all the interacting factors into a single factor with multiple levels and
#' then performs a Tukey test to compare the groups and group them in significantly
#' different levels. It also plots a boxplot with labels corresponding to significantly
#' different groups.
#'
#'@param model A final model whith the interaction terms of interest
#'@param terms The interaction terms of interest as a character vector
#'@param data The data that was used to fit the model
#'@param ylab Y-axis title (response variable).
#'@return
#'
#'@export
my_tukey <- function(model, terms, data, ylab){

  # First detect and remove from the list of variables to plot, those that are continuous
  int_factor <- select_if(data[, terms], is.factor)
  # Create a collapsed treatment factor of factorial interactions.
  data2 <- data %>%
    mutate(treatment = factor(apply(int_factor, 1, paste, collapse = "/")))

  # Run the same model but with treatment
  tuk_model <- update(model, . ~ treatment, data = data2,
                      evaluate = TRUE)

   #Calculate Tukey contrasts
   mod_means_contr <- emmeans::emmeans(object = tuk_model,
                                      pairwise ~ "treatment",
                                      adjust = "tukey",
                                      data = data2)

  # Create compact letter displays (CLD, letters) to group factor levels.
  mod_means <- multcomp::cld(object = mod_means_contr$emmeans,
                             Letters = letters)
  # Plot the results
  ggplot(data = mod_means,
         aes(x = treatment, y = emmean, col = .group)) +
    geom_errorbar(aes(ymin = asymp.LCL,
                      ymax = asymp.UCL),
                  width = 0.25, size = 0.7) +
    geom_point(size = 3) +
    geom_text(aes(label = gsub(" ", "", .group), col = .group),
              position = position_nudge(y = mod_means$asymp.UCL - mod_means$emmean + 0.2)) +
    theme_bw() +
    theme(legend.position = "none",
          text = element_text(size = 13),
          axis.text.x = element_text(angle = -90, hjust = 0, size = 12)) +
    labs(x = 'Treatment', y = ylab,
         title = glue::glue_collapse(names(int_factor), sep = ' x '))

}

#' Plotting of main effects
#'
#' This function plots the main effects. By default it plots all main effects,
#' but it can also plot only the ones that are not involved in an interaction.
#'
plot_mains <- function(model, terms, data, ylab, type = 'all'){

  X <- switch(type == 'all',
              all = terms$main,
              main_only = terms$main_only)

  # First detect and remove from the list of variables to plot, those that are continuous
  int_factor <- select_if(data[, X], is.factor)

  # Create formula
  rhs <- paste(X, collapse = ' + ')

  # Run the same model but with only mains
  mains_model <- update(model, as.formula(paste('. ~', rhs)), data = data,
                      evaluate = TRUE)

  mod_means <- emmeans::emmeans(object = mains_model,
                                      pairwise ~ Water,
                                      data = data)

  # Create dataset to populate with marginal means.
  temp <- data.frame(term = X)

  #data_emm <-emmeans::emmeans(mains_model, terms$main)
  #Calculate marginal means
  #data_emm <- temp %>%
  #  mutate(emmeans = pmap(list(object = mains_model,
  #                            pairwise = term,
  #                            data = temp),
  #                       emmeans::emmeans))


  mod_means <- as.data.frame(mod_means$emmeans)

  # Plot the results
  ggplot(data = mod_means,
         aes(x = Water, y = emmean)) +
    geom_errorbar(aes(ymin = asymp.LCL,
                      ymax = asymp.UCL),
                  width = 0.25, size = 0.7) +
    geom_point(size = 3) +
    theme_bw() +
    theme(legend.position = "none",
          text = element_text(size = 13),
          axis.text.x = element_text(angle = -90, hjust = 0, size = 12)) +
    labs(x = 'Population', y = ylab)
}
