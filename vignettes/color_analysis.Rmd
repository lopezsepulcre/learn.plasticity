---
title: "Plasticity in Male Coloration"
author: "Swanne Gordon and Andrés López-Sepulcre"
date: "2024-02-10"
output: html_document
---

```{r setup, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE)
library(tidyverse)
library(glmmTMB)
library(ggeffects)
library(stargazer)
library(patchwork)
library(car)
library(emmeans)
  
  plot_results <- function(model, trait){

predictions <- ggpredict(model,
                          ci.lvl = 0.48,
                          terms = c('pop', 
                                    'pred_trt',
                                    'tutor_pop'))

ggplot(data = predictions, 
       aes(x = facet, y = predicted,
           shape = group, linetype = group)) +
  geom_point(aes(group = group), 
             size = 3, position =  position_dodge(0.5)) +
  geom_line(aes(x = facet, y = predicted, 
                group = group, linetype = group), 
            position =  position_dodge(0.5)) +
  geom_errorbar(aes(ymin = conf.low, 
                    ymax = conf.high), width = 0.1,
                position = position_dodge(0.5),
                linetype =1) +
  facet_grid(. ~ x, scales = "free_x",
             labeller = as_labeller(c(`AH` = "High Predation",
                                    `AL` = "Low Predation"))) +
  xlab('') +
  scale_x_discrete(labels=c("0" = "solo",
                            "AH" = "HP",
                            "AL" = "LP")) +
  scale_shape_manual(name = "Predator cues", 
                   labels=c("Y" = "pred+",
                            "N" = "pred-"),
                   values = c(1, 16)) +
  scale_linetype_manual(name = "Predator cues", 
                   labels=c("Y" = "pred+",
                            "N" = "pred-"),
                   values=c(2,1)) +
  ylab(trait) + xlab("") +
  theme_bw() +
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = "bottom",
        strip.background = element_blank(),
        strip.text = element_text(size = 14))
}

```

### Data Preparation

There are two datasets containing the relevant data:

- `Plasticity Color Data.csv` contains the color measurements. We will call it `data_col`.
- `F2_Data_122821.csv` contains the metadata of the experiment. We will call it `data_exp`.

Because color should not respond to the immediate environment (at least not like we're measuring it), we will only use the data from the $pred^-$ treatment.

```{r}
data_col<-read.csv("../data_raw/Plasticity Color Data.csv")
data_exp<-read.csv("../data_raw/F2_Data_122821.csv") %>%
  filter(assay_water == "pred-")

data <- right_join (data_exp, data_col, by = "ID") %>%
  mutate(tutor_pop = factor(ifelse(is.na(tutor_pop), "0", tutor_pop)), ordered = TRUE)

contrasts(data$tutor_pop) <- contr.helmert(3)
contrasts(data$tutor_pop) <- matrix(c(-2/3,1/3,1/3,
                                      0,-0.5,0.5), nrow = 3, ncol = 2)

```

### Body Size

```{r}
model_size <- glmmTMB(Body.Area ~ 
                          (pop + pred_trt + tutor_pop)^3 +
                          (1|mom_ID), data)


kable(summary(model_size)$coefficients)

#Anova(model_size)
#emmeans(model_size, ~ tutor_pop|pop|pred_trt, type = "response") %>% pairs

```


```{r}
plot_sl <- plot_results(model_size, trait = expression(paste("Body Area (", mm^2,")")))
plot_sl
```
### Orange Body

```{r}
model_orange <- glmmTMB(Body.Orange.Relative.Area ~ 
                          pop * tutor_pop + pred_trt +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_orange)$coefficients)

```


```{r}
plot_ob <- plot_results(model_orange, trait = "Relative Orange Area")
plot_ob
```

### Xanthophore Body

```{r}
model_xantho <- glmmTMB(Body.Xanthophore.Relative.Area ~ 
                          (pop + pred_trt + tutor_pop)^2 - pop : pred_trt +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_xantho)$coefficients)
```


```{r}
plot_xb <- plot_results(model_xantho, trait = "Body Xanthophore")
plot_xb
```

### Black Body

```{r}
model_black <- glmmTMB(Body.Black.Relative.Area ~ 
                          pop + pred_trt * tutor_pop +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_black)$coefficients)

```


```{r}
plot_bb <- plot_results(model_black, trait = "Relative Black Area")
plot_bb
```

### Melanistic Body

```{r}
model_mela <- glmmTMB(Body.Melanistic.Relative.Area ~ 
                          (pop + pred_trt + tutor_pop)^2 - pop : tutor_pop +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_mela)$coefficients)

```


```{r}
plot_mb <- plot_results(model_mela, trait = "Melanistic Body")
plot_mb
```

### Orange Tail

```{r}
model_otail <- glmmTMB(Tail.Orange.Relative.Area ~ 
                          pop + pred_trt + tutor_pop +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_otail)$coefficients)
```


```{r}
plot_ot <- plot_results(model_otail, trait = "Orange Tail")
plot_ot
```

### Black Tail

```{r}
model_btail <- glmmTMB(Tail.Black.Relative.Area ~ 
                          pop + pred_trt * tutor_pop +
                          (1|mom_ID), 
                        family = 'ordbeta', data)

kable(summary(model_btail)$coefficients)
```


```{r}
plot_bt <- plot_results(model_btail, trait = "Black Tail")
plot_bt
```

### Orange Number

```{r}
model_onr <- glmmTMB(Body.Orange.Number ~ 
                          (pop * pred_trt + tutor_pop)^2 +
                          (1|mom_ID), 
                        family = 'genpois', data)

kable(summary(model_onr)$coefficients)
```


```{r}
plot_on <- plot_results(model_onr, trait = "Number of Orange Spots")
plot_on
```

### Black Number

```{r}
model_bnr <- glmmTMB(Body.Black.Number ~ 
                          pred_trt + pop * tutor_pop +
                          (1|mom_ID), 
                        family = 'genpois', data)

kable(summary(model_bnr)$coefficients)
```

```{r}
plot_bn <- plot_results(model_bnr, trait = "Number of Black Spots")
plot_bn
```

```{r}
sjPlot::tab_model(model_orange, model_xantho,
                  model_black, model_mela,
                  model_otail, model_btail,
                  model_onr, model_bnr,
                  show.se = TRUE, 
                  show.ci = FALSE, 
                  collapse.se = TRUE,
                  show.icc = FALSE,
                  show.r2 = FALSE)
```

```{r, fig.height = 20, fig.width = 5}
plot_ob + plot_xb + 
plot_bb + plot_mb +
plot_ot + plot_bt +
plot_on + plot_bn +
  plot_layout(ncol = 1,
              axis_titles = "collect",
              guides = "collect") +
  plot_annotation(title = "                 High Predation                   Low Predation") &
  theme(legend.position = 'bottom') 
```

### FINAL MS FIGURES
```{r, fig.height = 3.5, fig.width = 5}
fig3 <- plot_sl+
  plot_layout(guides = "collect") & labs(x = "Tutor") &
  theme(legend.position = "bottom")
fig3
```


```{r, fig.height = 6, fig.width = 5}

fig4 <- (plot_on / plot_ob) +
  plot_layout(guides = "collect") & labs(x = "Tutor") &
  theme(legend.position = "bottom")
fig4

fig5 <- (plot_bn / plot_bb) +
  plot_layout(guides = "collect") & labs(x = "Tutor") &
  theme(legend.position = "bottom")
fig5
```

